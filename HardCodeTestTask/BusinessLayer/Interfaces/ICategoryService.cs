using BusinessLayer.Dtos.Categories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Interfaces
{
    public interface ICategoryService
    {
        Task CreateCategory(CreateCategoryRequestDto requestDto);
    }
}
