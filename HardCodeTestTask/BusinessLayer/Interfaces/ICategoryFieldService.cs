using BusinessLayer.Dtos.CategoryFields;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Interfaces
{
    public interface ICategoryFieldService
    {
        Task CreateCategoryField(CreateCategoryFieldRequestDto requestDto);
    }
}
