using BusinessLayer.Dtos.Products;
using DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Interfaces
{
    public interface IProductService
    {
        Task CreateProduct(CreateProductRequestDto requestDto);

        Task<List<Product>> GetProducts();

        Task<Product> GetByProductId(string id);
    }
}
