using AutoMapper;
using BusinessLayer.Dtos.Categories;
using BusinessLayer.Dtos.CategoryFields;
using BusinessLayer.Interfaces;
using DAL.Entities;
using DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Services
{
    public class CategoryFieldService : ICategoryFieldService
    {
        private readonly IRepository<Product> _productRepository;
        private readonly ICategoryRepository _categoryRepository;
        private readonly ICategoryFieldRepository _categoryFieldRepository;
        private readonly IMapper _mapper;

        public CategoryFieldService(IRepository<Product> productRepository, ICategoryRepository categoryRepository, ICategoryFieldRepository categoryFieldRepository, IMapper mapper)
        {
            _productRepository = productRepository;
            _categoryRepository = categoryRepository;
            _categoryFieldRepository = categoryFieldRepository;
            _mapper = mapper;
        }

        public async Task CreateCategoryField(CreateCategoryFieldRequestDto requestDto)
        {
            var categoryField = _mapper.Map<CategoryField>(requestDto);
            var category = await _categoryRepository.GetByName(requestDto.CategoryName);
            categoryField.CategoryId = category.Id;
            var addedField = await _categoryFieldRepository.Add(categoryField);
            await _categoryFieldRepository.Save();
        }
    }
}
