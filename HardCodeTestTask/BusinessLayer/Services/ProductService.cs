using AutoMapper;
using BusinessLayer.Dtos.Products;
using BusinessLayer.Interfaces;
using DAL.Entities;
using DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Services
{
    public class ProductService : IProductService
    {
        private readonly IRepository<Product> _productRepository;
        private readonly ICategoryRepository _categoryRepository;
        private readonly ICategoryFieldRepository _categoryFieldRepository;
        private readonly IProductFieldRepository _productFieldRepository;
        private readonly IMapper _mapper;

        public ProductService(IRepository<Product> productRepository, ICategoryRepository categoryRepository, ICategoryFieldRepository categoryFieldRepository, IMapper mapper, IProductFieldRepository productFieldRepository)
        {
            _productRepository = productRepository;
            _categoryRepository = categoryRepository;
            _categoryFieldRepository = categoryFieldRepository;
            _mapper = mapper;
            _productFieldRepository = productFieldRepository;
        }

        public async Task CreateProduct(CreateProductRequestDto requestDto)
        {
            var product = _mapper.Map<Product>(requestDto);
            product.Id = Guid.NewGuid().ToString();
            var category = await _categoryRepository.GetByName(requestDto.CategoryName);
            product.CategoryId = category.Id;
            var addedProduct = await _productRepository.Add(product);
            await _productRepository.Save();
            var fields = await _categoryFieldRepository.GetByCategoryId(category.Id);
            foreach (var field in fields)
            {
                var productField = requestDto.Fields.SingleOrDefault(q => q.Name == field.Name);
                await _productFieldRepository.Add(new ProductField
                {
                    Id = Guid.NewGuid().ToString(),
                    CategoryFieldId = field.Id,
                    ProductId = product.Id,
                    Value = productField.Name is null ? string.Empty : productField.Name,
                });
            }
        }

        public async Task<List<Product>> GetProducts()
        {
            return await _productRepository
                .GetAll()
                .ToListAsync();
        }

        public async Task<Product> GetByProductId(string id)
        {
            return await _productRepository
                .GetById(id);
        }
    }
}
