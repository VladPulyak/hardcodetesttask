using AutoMapper;
using BusinessLayer.Dtos.Categories;
using BusinessLayer.Interfaces;
using DAL.Entities;
using DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Services
{
    public class CategoryService : ICategoryService
    {
        private readonly IRepository<Product> _productRepository;
        private readonly ICategoryRepository _categoryRepository;
        private readonly ICategoryFieldRepository _categoryFieldRepository;
        private readonly IMapper _mapper;

        public CategoryService(IRepository<Product> productRepository, ICategoryRepository categoryRepository, ICategoryFieldRepository categoryFieldRepository, IMapper mapper)
        {
            _productRepository = productRepository;
            _categoryRepository = categoryRepository;
            _categoryFieldRepository = categoryFieldRepository;
            _mapper = mapper;
        }

        public async Task CreateCategory(CreateCategoryRequestDto requestDto)
        {
            var category = _mapper.Map<Category>(requestDto);
            category.Id = Guid.NewGuid().ToString();
            var addedCategory = await _categoryRepository.Add(category);
            await _categoryRepository.Save();
            var fields = requestDto.Fields.Select(q => new CategoryField()
            {
                Id = Guid.NewGuid().ToString(),
                Name = q.Name,
                CategoryId = category.Id
            }).ToList();
            await _categoryFieldRepository.AddRange(fields);
            await _categoryRepository.Save();
        }
    }
}
