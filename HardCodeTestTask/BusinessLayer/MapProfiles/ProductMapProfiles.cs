using AutoMapper;
using BusinessLayer.Dtos.Products;
using DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.MapProfiles
{
    public class ProductMapProfiles : Profile
    {
        public ProductMapProfiles()
        {
            CreateMap<CreateProductRequestDto, Product>().ReverseMap();
        }
    }
}
