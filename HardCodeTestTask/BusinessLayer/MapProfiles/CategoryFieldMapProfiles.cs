using AutoMapper;
using BusinessLayer.Dtos.CategoryFields;
using DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.MapProfiles
{
    public class CategoryFieldMapProfiles : Profile
    {
        public CategoryFieldMapProfiles()
        {
            CreateMap<CreateCategoryFieldRequestDto, CategoryField>().ReverseMap();
        }
    }
}
