using AutoMapper;
using BusinessLayer.Dtos.Categories;
using DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.MapProfiles
{
    public class CategoryMapProfiles : Profile
    {
        public CategoryMapProfiles()
        {
            CreateMap<CreateCategoryRequestDto, Category>()
                .ForMember(q => q.Fields, w => w.Ignore());
        }
    }
}
