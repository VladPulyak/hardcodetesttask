using BusinessLayer.Dtos.CategoryFields;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Dtos.Categories
{
    public class CreateCategoryRequestDto
    {
        public string? Name { get; set; }

        public List<ProductFieldDto>? Fields { get; set; }
    }
}
