using BusinessLayer.Dtos.CategoryFields;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Dtos.Products
{
    public class CreateProductRequestDto
    {
        public string? Name { get; set; }

        public string? CategoryName { get; set; }

        public List<ProductFieldDto>? Fields { get; set; }
    }
}
