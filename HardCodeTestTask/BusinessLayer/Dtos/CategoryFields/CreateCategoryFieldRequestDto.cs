using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Dtos.CategoryFields
{
    public class CreateCategoryFieldRequestDto
    {
        public string? Name { get; set; }

        public string? CategoryName { get; set; }
    }
}
