using BusinessLayer.Interfaces;
using BusinessLayer.MapProfiles;
using BusinessLayer.Services;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static void AddServices(this IServiceCollection services)
        {
            services.AddScoped<IProductService, ProductService>();
            services.AddScoped<ICategoryService, CategoryService>();
            services.AddScoped<ICategoryFieldService, CategoryFieldService>();
            services.AddAutoMapper(typeof(CategoryFieldMapProfiles), typeof(CategoryMapProfiles), typeof(ProductMapProfiles));
        }
    }
}
