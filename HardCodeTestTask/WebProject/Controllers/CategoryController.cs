﻿using BusinessLayer.Dtos.Categories;
using BusinessLayer.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebProject.Controllers
{
    [Route("api")]
    [ApiController]
    public class CategoryController : ControllerBase
    {
        private readonly ICategoryService _categoryService;

        public CategoryController(ICategoryService categoryService)
        {
            _categoryService = categoryService;
        }

        [HttpPost("categories")]
        public async Task<ActionResult> CreateCategory(CreateCategoryRequestDto requestDto)
        {
            try
            {
                await _categoryService.CreateCategory(requestDto);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return Ok("Category added successfully!");
        }
    }
}
