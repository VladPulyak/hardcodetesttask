﻿using BusinessLayer.Dtos.Products;
using BusinessLayer.Interfaces;
using DAL.Exceptions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebProject.Controllers
{
    [Route("api")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly IProductService _productService;

        public ProductController(IProductService productService)
        {
            _productService = productService;
        }

        [HttpPost("products")]
        public async Task<ActionResult> CreateProduct(CreateProductRequestDto requestDto)
        {
            try
            {
                await _productService.CreateProduct(requestDto);
            }
            catch (ObjectNotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return Ok("Product added successfully!");
        }

        [HttpGet("products")]
        public async Task<ActionResult> GetProducts()
        {
            try
            {
                return Ok(await _productService.GetProducts());
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("products/by-id")]
        public async Task<ActionResult> GetByProductId(string id)
        {
            try
            {
                return Ok(await _productService.GetByProductId(id));
            }
            catch (ObjectNotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
