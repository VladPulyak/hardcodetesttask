﻿using BusinessLayer.Dtos.CategoryFields;
using BusinessLayer.Interfaces;
using DAL.Exceptions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebProject.Controllers
{
    [Route("api")]
    [ApiController]
    public class CategoryFieldController : ControllerBase
    {
        private readonly ICategoryFieldService _categoryFieldService;

        public CategoryFieldController(ICategoryFieldService categoryFieldService)
        {
            _categoryFieldService = categoryFieldService;
        }

        [HttpPost("fields")]
        public async Task<ActionResult> CreateField(CreateCategoryFieldRequestDto requestDto)
        {
            try
            {
                await _categoryFieldService.CreateCategoryField(requestDto);
            }
            catch (ObjectNotFoundException ex)
            {
                return NotFound(ex.Message);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            return Ok("Field added succesfully!");
        }
    }
}
