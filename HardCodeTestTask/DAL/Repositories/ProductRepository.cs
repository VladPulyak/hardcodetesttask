using DAL.Entities;
using DAL.Exceptions;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    public class ProductRepository : Repository<Product>
    {
        public ProductRepository(TestTaskDbContext context) : base(context)
        {

        }
        public override IQueryable<Product> GetAll()
        {
            return _set
                .Include(q => q.Category)
                .Include(q => q.Category.Fields)
                .AsNoTracking();
        }

        public override async Task<Product> GetById(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                throw new InvalidArgumentException("Invalid argument");
            }

            var entity = await _set
                .Where(q => q.Id == id)
                .Include(q => q.Category)
                .Include(q => q.Category.Fields)
                .SingleOrDefaultAsync();
            if (entity is null)
            {
                throw new ObjectNotFoundException("Object with this id is not found");
            }
            return entity;

        }
    }
}
