using DAL.Entities;
using DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    public class CategoryFieldRepository : Repository<CategoryField>, ICategoryFieldRepository
    {
        public CategoryFieldRepository(TestTaskDbContext context) : base(context)
        {

        }

        public Task AddRange(List<CategoryField> fields)
        {
            return _set.AddRangeAsync(fields);
        }

        public async Task<List<CategoryField>> GetByCategoryId(string id)
        {
            return await _set
                .Where(q => q.CategoryId == id)
                .ToListAsync();
        }
    }
}
