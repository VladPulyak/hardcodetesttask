using DAL.Entities;
using DAL.Exceptions;
using DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    public class CategoryRepository : Repository<Category>, ICategoryRepository
    {
        public CategoryRepository(TestTaskDbContext context) : base(context)
        {

        }

        public async Task<Category> GetByName(string name)
        {
            var category = await _set
                .SingleOrDefaultAsync(q => q.Name == name);
            if (category is null)
            {
                throw new ObjectNotFoundException("Category with this name is not found");
            }
            return category;
        }
    }
}
