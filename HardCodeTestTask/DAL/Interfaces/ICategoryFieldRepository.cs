using DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Interfaces
{
    public interface ICategoryFieldRepository : IRepository<CategoryField>
    {
        Task AddRange(List<CategoryField> fields);

        Task<List<CategoryField>> GetByCategoryId(string id);
    }
}
