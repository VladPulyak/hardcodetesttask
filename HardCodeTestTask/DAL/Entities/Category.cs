using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{
    public class Category
    {
        public string? Id { get; set; }

        public string? Name { get; set; }

        public ICollection<CategoryField>? Fields { get; set; }

        public ICollection<Product>? Products { get; set; }
    }
}
