using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{
    public class CategoryField
    {
        public string? Id { get; set; }

        public string? CategoryId { get; set; }

        public string? Name { get; set; }

        public Category? Category { get; set; }

        public ProductField? ProductField { get; set; }
    }
}
