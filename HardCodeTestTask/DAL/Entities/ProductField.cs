using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Entities
{
    public class ProductField
    {
        public string? Id { get; set; }

        public string? ProductId { get; set; }

        public string? CategoryFieldId { get; set; }

        public string? Value { get; set; }

        public Product? Product { get; set; }

        public CategoryField? CategoryField { get; set; }
    }
}
