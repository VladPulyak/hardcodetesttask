using DAL.Entities;
using DAL.Interfaces;
using DAL.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static void AddDbConnection(this IServiceCollection services, string connectionString)
        {
            services.AddDbContext<TestTaskDbContext>(services =>
            {
                services.UseNpgsql(connectionString);
            });
        }

        public static void AddEntityDependencies(this IServiceCollection services)
        {
            services.AddScoped<IRepository<Product>, ProductRepository>();
            services.AddScoped<ICategoryRepository, CategoryRepository>();
            services.AddScoped<ICategoryFieldRepository, CategoryFieldRepository>();
            services.AddScoped<IProductFieldRepository, ProductFieldRepository>();
        }
    }
}
