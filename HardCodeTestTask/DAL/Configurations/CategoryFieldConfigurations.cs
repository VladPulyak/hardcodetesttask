using DAL.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Configurations
{
    public class CategoryFieldConfigurations : IEntityTypeConfiguration<CategoryField>
    {
        public void Configure(EntityTypeBuilder<CategoryField> builder)
        {
            builder.ToTable("CategoryFields");
            builder.HasKey(q => q.Id);
            builder.Property(q => q.Name).IsRequired().HasMaxLength(50);
            builder.HasOne(q => q.Category).WithMany(w => w.Fields).HasForeignKey(q => q.CategoryId).OnDelete(DeleteBehavior.Cascade);
        }
    }
}
