using DAL.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Configurations
{
    public class ProductFieldConfigurations : IEntityTypeConfiguration<ProductField>
    {
        public void Configure(EntityTypeBuilder<ProductField> builder)
        {
            builder.ToTable("ProductFields");
            builder.HasKey(x => x.Id);
            builder.HasOne(q => q.Product).WithMany(w => w.ProductFields).HasForeignKey(q => q.ProductId).OnDelete(DeleteBehavior.Cascade);
            builder.HasOne(q => q.CategoryField).WithOne(w => w.ProductField).HasForeignKey<ProductField>(q => q.CategoryFieldId).OnDelete(DeleteBehavior.Cascade);
        }
    }
}
