using DAL.Configurations;
using DAL.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class TestTaskDbContext : DbContext
    {
        public TestTaskDbContext()
        {
            
        }

        public TestTaskDbContext(DbContextOptions<TestTaskDbContext> options) : base(options)
        {

        }

        public DbSet<Product>? Products { get; set; }

        public DbSet<Category>? Categories { get; set; }
        
        public DbSet<CategoryField>? CategoryFields { get; set; }

        public DbSet<ProductField>? ProductFields { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new CategoryConfigurations());
            modelBuilder.ApplyConfiguration(new ProductConfigurations());
            modelBuilder.ApplyConfiguration(new CategoryFieldConfigurations());
            modelBuilder.ApplyConfiguration(new ProductFieldConfigurations());
            base.OnModelCreating(modelBuilder);
        }
    }
}
